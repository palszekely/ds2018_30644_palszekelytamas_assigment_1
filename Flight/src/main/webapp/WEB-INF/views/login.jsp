<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%> 
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Login</title>
<style type="text/css">



body{
background-color: lightblue;
background-size:cover;
font-family:Arial;
text-align:center;
}
</style>
	</head>
	<body>
	
	<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
      <font color="red">
        Your login attempt was not successful due to <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
      </font>
</c:if>

		<font color="red">${message}</font>
		<form:form id="loginForm" method="post" action="login" modelAttribute="loginBean">
			<form:label path="username">Username</form:label>
			<form:input id="username" name="username" path="" /><br>
			<form:label path="password">Password</form:label>
			<form:password id="password" name="password" path="" /><br>
			<input type="hidden"
			name="${_csrf.parameterName}" value="${_csrf.token}" />
			<input type="submit" value="Login" />
		</form:form>
	</body>
</html>
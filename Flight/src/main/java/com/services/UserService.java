package com.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.UserDao;
import com.entities.User;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class UserService {
	
	    @Autowired
	    UserDao userDao;
	    @Transactional
	    public boolean saveOrUpdate(User users) {
	        return userDao.saveOrUpdate(users);
	    }
	    
	    public int isValidUser(String username,String password)
	    {
	    	return userDao.isValidUser(username,password);
	    }
	    
	    public int yetexist(String username)
	    {
	    	return userDao.yetexist(username);
	    }
	   

}

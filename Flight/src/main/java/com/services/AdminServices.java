package com.services;

import java.util.List;
import org.springframework.stereotype.Component;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.ComponentScan;

import com.dao.AdminDao;
import com.entities.Flight;
@Service
@Transactional
public class AdminServices {

	@Autowired
	    AdminDao adminDao;
	    
	   
	    
	    @Transactional
	    public boolean saveOrUpdate(Flight flight) {
	        return adminDao.saveOrUpdate(flight);
	    }
	    
	    @Transactional
	    public List<Flight> listFlights() {
	    	return adminDao.listsFlights();
	    }
	
	    @Transactional
	    public boolean deleteFlights(Integer id) {
	        return adminDao.deleteFlight(id);
	    }
	   
	    
}
package com.dao;

import java.util.List;
import org.hibernate.*;
import org.hibernate.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.entities.User;


import org.hibernate.SharedSessionContract;
@Repository
@Transactional
public class UserDao  {
	
	    @Autowired
	    SessionFactory session;
	 
	    
	    public boolean saveOrUpdate(User users) {
	        session.getCurrentSession().saveOrUpdate(users);
	        return true;
	    }
	    
	    public int isValidUser(String uname,String pword)
	    {
	    	try {
	    	Query  q =  
	    			 session.getCurrentSession().createQuery("FROM User u where u.username=:username and u.password=:password "); 
	        q.setParameter("username", uname); 
	        q.setParameter("password", pword);
	        User user = (User) q.uniqueResult();
	        
	        if (user.getRole()==0 || user.getRole()==1)
	        return user.getRole();
	        else return -1;
	    	}
	    	catch(Exception e) {
	    		
	    		  System.out.println(e); 
	    		return -1;
	    	}
	        
	    	
	    
	    }
	    
	    public int yetexist(String uname)
	    {
	    	try {
	    	Query  q =  
	    			 session.getCurrentSession().createQuery("FROM usertable u where u.user_name=:username"); 
	        q.setParameter("username", uname); 
	        User user = (User) q.uniqueResult();
	        if (user.getRole()==0 || user.getRole()==1)
	         return -1;
	        else return 0;
	    	}
	    	catch(Exception e) {
	    		
	    		return 1;
	    	}
	        
	    	
	    
	    }
}
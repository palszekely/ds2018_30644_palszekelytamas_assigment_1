package com.dao;

import java.util.List;
import org.hibernate.*;
import org.hibernate.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.entities.Flight;

import org.hibernate.SharedSessionContract;
import java.util.*;
@Repository
@Transactional
public class AdminDao {
	
	@Autowired
	  SessionFactory session;
	
	
	 
	    public boolean saveOrUpdate(Flight flight) {
	        session.getCurrentSession().saveOrUpdate(flight);
	        return true;
	    }
	  
	   
	   
	   public List<Flight> listsFlights() {
			  
			  
		    	List<Flight> flightsList=new ArrayList<Flight>() ;
		     Query q=session.getCurrentSession().createQuery("From Flight");

		    	flightsList=q.list();
		    	
		    	
		    	return flightsList;
		    	
		    }
	   
	   public boolean deleteFlight(Integer id) {
		   Query query= session.getCurrentSession().createQuery("delete from Flight where flight_id=:id");
					query.setParameter("id", id);
					int result = query.executeUpdate();
					return true;
	    }
	   
	
	    

}
package com.controllers;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dao.AdminDao;
import com.entities.Flight;
import com.entities.User;
import com.services.AdminServices;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
@Controller
public class RegUserController {
	
	/*
	@RequestMapping(value="/welcome",method=RequestMethod.GET)
	public ModelAndView listContact(ModelAndView model) throws IOException,SQLException,ClassNotFoundException{
		 AdminDao carsDAO=new AdminDao();
	    List<Cars> carsList = carsDAO.listCars();
	    model.addObject("carsList", carsList);
		
	 
	    return model;
	}
	*/
	
	@Autowired
    AdminServices adminServices;
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView listContact(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView("welcome");
		    List<Flight> flightsList = this.adminServices.listFlights();
		    model.addObject("flightsList", flightsList);
			
		 
		    return model;
    }
	
	

	

}
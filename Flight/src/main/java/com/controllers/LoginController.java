package com.controllers;

import com.dao.UserDao;
import com.entities.User;
import com.services.UserService;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LoginController {
	
	@Autowired
    UserService userService;
	
	
	    @RequestMapping(value="/login",method=RequestMethod.GET)
	    public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response)
	    {

	        ModelAndView model = new ModelAndView("login");
	        User loginBean =new User();
	        model.addObject("loginBean", loginBean);
	        return model;
	    }
	  
		

	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ModelAndView executeLogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("loginBean")User loginBean)
	{
			ModelAndView model= null;
			
			try
			{
					int isValidUser = userService.isValidUser(loginBean.getUsername(), loginBean.getPassword());
					if(isValidUser==0)
					{
							System.out.println("User Login Successful");
							request.setAttribute("loggedInUser", loginBean.getUsername());
							return new ModelAndView("redirect:/welcome");
							
					}
					
					else if(isValidUser==1) {
						return new ModelAndView("redirect:/adminpart");
						
					}
					else 
					{
						    model = new ModelAndView("redirect:/invalid");
							request.setAttribute("message", isValidUser);
							
					}

			}
			catch(Exception e)
			{
					e.printStackTrace();
			}
			
			
		return model;
	}

}
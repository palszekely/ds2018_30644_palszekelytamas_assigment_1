package com.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Flight;
import com.entities.Deleteid;
import com.services.AdminServices;
import org.springframework.context.annotation.ComponentScan;
@Controller
public class AdminUpdateController {
	
	@Autowired
     AdminServices adminServices;
    
 
    
	@RequestMapping(value = "/adminupdate", method = RequestMethod.GET)
    public ModelAndView getPage1(HttpServletRequest request, HttpServletResponse response) {
    	 ModelAndView model = new ModelAndView("adminupdate");
		 Flight flightdao=new Flight();
		 model.addObject("flightsu", flightdao);
        return model;
    }
	
	
	
	 @RequestMapping(value = "/adminupdate", method = RequestMethod.POST)
	    public  Map<String, Object> getSaved1(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("flightsu") Flight flight) {
	        Map<String, Object> map = new HashMap<String, Object>();  
	        if (adminServices.saveOrUpdate(flight)) {
	            map.put("status", "200");
	            map.put("message", "Your record have been saved successfully");
	        }
	 
	        return map;
	    }
	 
	 
	
	 

	
}
package com.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "flighttable")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "flight_id")
    private int flight_id;
    
    @Column(name = "airplane_type")
    private String airplane_type;
 
    @Column(name = "dep_city")
    private String dep_city;
    
    @Column(name = "dep_date")
    private String dep_date;
    
    @Column(name = "arr_city")
    private String arr_city;
    
    @Column(name = "arr_date")
    private String arr_date;
    
    @Column(name = "fl_nr")
    private int fl_nr;
    
    @Column(name = "dclo")
    private int dclo;
    
    @Column(name = "dcla")
    private int dcla;
    
    @Column(name = "aclo")
    private int aclo;
    
    @Column(name = "acla")
    private int acla;
 
    
    public Flight(int k,String f,String s,String p ,String b,String j,int nr,int x,int y,int z ,int w) {
    	this.flight_id=k;
    	this.airplane_type=f;
    	this.dep_city=s;
    	this.dep_date=p;
    	this.arr_city=b;
    	this.arr_date=j;
    	this.fl_nr=nr;
    	this.dclo=x;
    	this.dcla=y;
    	this.aclo=z;
    	this.dclo=w;
    }
    
    public int getDclo() {
		return dclo;
	}

	public void setDclo(int dclo) {
		this.dclo = dclo;
	}

	public int getDcla() {
		return dcla;
	}

	public void setDcla(int dcla) {
		this.dcla = dcla;
	}

	public int getAclo() {
		return aclo;
	}

	public void setAclo(int aclo) {
		this.aclo = aclo;
	}

	public int getAcla() {
		return acla;
	}

	public void setAcla(int acla) {
		this.acla = acla;
	}

	public int getFlight_id() {
		return flight_id;
	}

	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}

	public String getAirplane_type() {
		return airplane_type;
	}

	public void setAirplane_type(String airplane_type) {
		this.airplane_type = airplane_type;
	}

	public String getDep_city() {
		return dep_city;
	}

	public void setDep_city(String dep_city) {
		this.dep_city = dep_city;
	}

	public String getDep_date() {
		return dep_date;
	}

	public void setDep_date(String dep_date) {
		this.dep_date = dep_date;
	}

	public String getArr_city() {
		return arr_city;
	}

	public void setArr_city(String arr_city) {
		this.arr_city = arr_city;
	}

	public String getArr_date() {
		return arr_date;
	}

	public void setArr_date(String arr_date) {
		this.arr_date = arr_date;
	}

	public int getFl_nr() {
		return fl_nr;
	}

	public void setFl_nr(int fl_nr) {
		this.fl_nr = fl_nr;
	}

	public Flight() {
    	super();
    }

	
    
    
}